import nltk

'''
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
'''

def get_words(str):
	tokens = nltk.word_tokenize(str)

	nouns=[]
	verbs=[]
	for word,pos in nltk.pos_tag(tokens):
		if (pos=='NNP' or pos=='NNPS' or pos=='NN' or pos=='NNS' or pos=='PRP' or pos=='PRP$'):
			nouns.append(word)
		elif (pos=='VB' or pos=='VBD' or pos=='VBG' or pos=='VBN' or pos=='VBP' or pos=='VBZ'):
			verbs.append(word)
	words = nouns + verbs

	return words