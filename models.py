from letstalk_server import db


class WordVideoReference(db.Model):
    __tablename__ = 'video_urls'

    word = db.Column(db.String(), primary_key=True)
    reference = db.Column(db.String())

    def __init__(self, word, reference):
        self.word = word
        self.reference = reference

    def __repr__(self):
        return '{ ' \
               '    word : {}' \
               '    reference : {}' \
               '}'.format(self.word, self.reference)

    def serialize(self):
        return {
            'word': self.word,
            'reference': self.reference
        }
