from models import WordVideoReference


def get_references_from_words(words):
    ref_dict = {}
    count = 1
    for word in words:
        word_ref = WordVideoReference.query.filter_by(word=word).first()
        if word_ref is not None:
            ref_dict[count] = word_ref.serialize()
            count += 1

    return ref_dict
