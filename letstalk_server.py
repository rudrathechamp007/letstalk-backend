import os
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from eng2isl_words import *

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


@app.route('/letstalk/getISL/')
def get_isl_urls():
    from word_to_reference import get_references_from_words

    sentence = request.args.get('sentence')
    sentence = sentence.strip('"')
    words = get_words(sentence)
    # url_array = []
    # for word in words:
    #     url_array.append('http://letstalk.com/{}.mp4'.format(word))
    # return {
    #     "urls": url_array
    # }
    urls_dict = get_references_from_words(words)
    return urls_dict


if __name__ == "__main__":
    app.run(host='0.0.0.0')
